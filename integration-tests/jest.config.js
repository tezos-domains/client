module.exports = {
    preset: 'ts-jest',
    testEnvironment: 'node',
    testTimeout: 30_000,
    moduleNameMapper: {
        '^@tezos-domains/core$': '<rootDir>/../packages/core/public_api.ts',
        '^@tezos-domains/resolver$': '<rootDir>/../packages/resolver/public_api.ts',
        '^@tezos-domains/manager$': '<rootDir>/../packages/manager/public_api.ts',
        '^@tezos-domains/client$': '<rootDir>/../packages/client/public_api.ts',
    },
    reporters: ['default', ['jest-junit', { outputDirectory: 'coverage' }]]
};
