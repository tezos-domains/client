// eslint-disable-next-line @typescript-eslint/ban-types
export type Constructable<T extends {}> = new (...args: unknown[]) => T;
export type Exact<T> = { [K in keyof T]: T[K] };
