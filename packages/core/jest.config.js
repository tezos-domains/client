/** @type {import('@ts-jest/dist/types').InitialOptionsTsJest} */

module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'node',
  collectCoverage: true,
  moduleNameMapper: {
    '^@tezos-domains/core$': '<rootDir>/public_api.ts',
  },
  coveragePathIgnorePatterns: ['/node_modules/', '/test/', '/dist/'],
  reporters: ['default', ['jest-junit', { outputDirectory: 'coverage' }]]
};