// import typescript from 'rollup-plugin-typescript2';
import typescript from '@rollup/plugin-typescript';

const pkg = require('./package.json');

export default {
    input: `public_api.ts`,
    output: [
        { file: pkg.main, name: 'tezosDomainsResolver', format: 'umd', sourcemap: true },
        { file: pkg.module, format: 'es', sourcemap: true },
    ],
    // Indicate here external modules you don't wanna include in your bundle (i.e.: 'lodash')
    external: ['@tezos-domains/core', '@taquito/taquito', '@taquito/utils', 'node-cache'],
    watch: {
        include: 'src/**',
    },
    plugins: [
        // Compile TypeScript files
        typescript({ tsconfig: './tsconfig.prod.json' }),

        // Resolve source maps to the original source
        // sourceMaps(),
    ],
};
