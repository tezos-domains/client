import { anyString, anything, imock, instance, verify, when } from '@johanblumenberg/ts-mockito';
import { DomainInfo, ReverseRecordDomainInfo, Tracer } from '@tezos-domains/core';
import { NameResolver } from '@tezos-domains/resolver';
import FakePromise from 'fake-promise';

import { NameNormalizingNameResolver } from '../../src/resolver/name-normalizing-name-resolver';

describe('NameNormalizingNameResolver', () => {
    let resolver: NameNormalizingNameResolver;
    let nameResolverMock: NameResolver;
    let tracerMock: Tracer;

    const ap: Promise<string> = new FakePromise();
    const np: Promise<string> = new FakePromise();
    let dp: Promise<DomainInfo> = Promise.resolve({} as DomainInfo);
    let rrp: Promise<ReverseRecordDomainInfo> = Promise.resolve({} as ReverseRecordDomainInfo);

    beforeEach(() => {
        nameResolverMock = imock<NameResolver>();
        tracerMock = imock<Tracer>();

        dp = new FakePromise();
        rrp = new FakePromise();

        when(tracerMock.trace(anything()));
        when(nameResolverMock.resolveDomainRecord(anyString())).thenReturn(dp);
        when(nameResolverMock.resolveNameToAddress(anyString())).thenReturn(ap);
        when(nameResolverMock.resolveReverseRecord(anyString())).thenReturn(rrp);
        when(nameResolverMock.resolveAddressToName(anyString())).thenReturn(np);

        resolver = new NameNormalizingNameResolver(instance(nameResolverMock), instance(tracerMock));
    });

    describe('resolveDomainRecord()', () => {
        it('should proxy call to decorated resolver', () => {
            const p = resolver.resolveDomainRecord('a');

            expect(p).toBe(dp);
        });

        it('should normalize domain name', () => {
            const p = resolver.resolveDomainRecord('AB.tez');

            verify(nameResolverMock.resolveDomainRecord('ab.tez')).called();
            expect(p).toBe(dp);
        });
    });

    describe('resolveNameToAddress()', () => {
        it('should proxy call to decorated resolver', () => {
            const p = resolver.resolveNameToAddress('a');

            expect(p).toBe(ap);
        });

        it('should normalize domain name', () => {
            const p = resolver.resolveNameToAddress('AB.tez');

            verify(nameResolverMock.resolveNameToAddress('ab.tez')).called();
            expect(p).toBe(ap);
        });
    });

    describe('resolveReverseRecord()', () => {
        it('should proxy call to decorated resolver', () => {
            const p = resolver.resolveReverseRecord('a');

            expect(p).toBe(rrp);
        });
    });

    describe('resolveAddressToName()', () => {
        it('should proxy call to decorated resolver', () => {
            const p = resolver.resolveAddressToName('a');

            expect(p).toBe(np);
        });
    });

    describe('clearCache()', () => {
        // eslint-disable-next-line jest/expect-expect
        it('should proxy call to decorated resolver', () => {
            resolver.clearCache();

            verify(nameResolverMock.clearCache()).called();
        });
    });
});
